package id.ac.ui.cs.advprog.tutorial2.observer.controller;

import id.ac.ui.cs.advprog.tutorial2.observer.core.Quest;
import id.ac.ui.cs.advprog.tutorial2.observer.service.GuildServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.*;
=======
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d

@Controller
public class ObserverController {

        @Autowired
        private GuildServiceImpl guildService;

<<<<<<< HEAD
        @GetMapping(value = "/create-quest")
=======
        @RequestMapping(value = "/create-quest", method = RequestMethod.GET)
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        public String createQuest(Model model){
                model.addAttribute("quest", new Quest());
                return "observer/questForm";
        }

<<<<<<< HEAD
        @PostMapping(value = "/add-quest")
=======
        @RequestMapping(value = "/add-quest", method = RequestMethod.POST)
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        public String addQuest(@ModelAttribute("quest") Quest quest) {
                guildService.addQuest(quest);
                return "redirect:/adventurer-list";
        }

<<<<<<< HEAD
        @GetMapping(value = "/adventurer-list")
=======
        @RequestMapping(value = "/adventurer-list", method = RequestMethod.GET)
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        public String getAdventurers(Model model){
                model.addAttribute("adventurers", guildService.getAdventurers());
                return "observer/adventurerList";
        }
}
