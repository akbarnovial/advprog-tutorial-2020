package id.ac.ui.cs.advprog.tutorial2.observer.service;

import id.ac.ui.cs.advprog.tutorial2.observer.core.*;
import id.ac.ui.cs.advprog.tutorial2.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial2.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

<<<<<<< HEAD

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                this.agileAdventurer = new AgileAdventurer("Agile", guild);
                this.knightAdventurer = new KnightAdventurer("Knight", guild);
                this.mysticAdventurer = new MysticAdventurer("Mystic", guild);
=======
        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                this.agileAdventurer = new Adventurer("Agile", guild);
                this.knightAdventurer = new Adventurer("Knight", guild);
                this.mysticAdventurer = new Adventurer("Mystic", guild);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        }

        @Override
        public void addQuest(Quest quest) {
                questRepository.save(quest);
<<<<<<< HEAD
                this.guild.addQuest(quest);
=======
                this.guild.broadcastQuest(quest);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return guild.getAdventurers();
        }
}
