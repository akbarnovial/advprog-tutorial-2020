package id.ac.ui.cs.advprog.tutorial2.observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MysticAdventurerTest {

    private Adventurer mysticAdventurer;

    @Mock
    private Guild guild;

    @BeforeEach
    public void setUp() {
<<<<<<< HEAD
        mysticAdventurer = new MysticAdventurer("Mystic", guild);
=======
        mysticAdventurer = new Adventurer("Mystic", guild);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptDeliveryQuestType() {
        Quest deliveryQuest = new Quest();
        deliveryQuest.setTitle("Dummy");
        deliveryQuest.setType("D");
<<<<<<< HEAD
        when(guild.getQuestType()).thenReturn(deliveryQuest.getType());
=======
        when(guild.getQuestType()).thenReturn(deliveryQuest.type);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        when(guild.getQuest()).thenReturn(deliveryQuest);

        mysticAdventurer.update();
        List<Quest> agileQuestList = mysticAdventurer.getQuests();

        assertThat(agileQuestList).contains(deliveryQuest);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptEscortQuestType() {
        Quest escortQuest = new Quest();
        escortQuest.setTitle("Dummy");
        escortQuest.setType("E");
<<<<<<< HEAD
        when(guild.getQuestType()).thenReturn(escortQuest.getType());
=======
        when(guild.getQuestType()).thenReturn(escortQuest.type);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d
        when(guild.getQuest()).thenReturn(escortQuest);

        mysticAdventurer.update();
        List<Quest> agileQuestList = mysticAdventurer.getQuests();

        assertThat(agileQuestList).contains(escortQuest);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptRumbleQuestType() {
        Quest rumbleQuest = new Quest();
        rumbleQuest.setTitle("Dummy");
        rumbleQuest.setType("R");
<<<<<<< HEAD
        when(guild.getQuestType()).thenReturn(rumbleQuest.getType());
=======
        when(guild.getQuestType()).thenReturn(rumbleQuest.type);
>>>>>>> e70008f85446a2033f0e0c95a94ee09e2f0a886d

        mysticAdventurer.update();
        List<Quest> agileQuestList = mysticAdventurer.getQuests();

        assertThat(agileQuestList).doesNotContain(rumbleQuest);
    }
}
