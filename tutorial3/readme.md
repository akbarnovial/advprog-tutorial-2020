# Tutorial 3
# Our Supply of Mana Ran Out so I Fix the Magic of Mana Distribution with Safe Multi-threading
What is this magic? How can it be so similar to things you already know? How did you lose your memory? How did you suddenly remember about unit testing and refactoring when the time needs them the most? Too many phenomenons have happened, yet you barely understand those phenomenons. You force yourself to study to understand those phenomenons.

You dedicate some time alone to understand the programming of this world: the magical engineering. You read the history of magic, how humanity started to use magic for their welfare, and the latest trend of magical engineering that apparently isn't late enough to compete with latest trend of programming in your past life. While you indulge yourself in your study, you find something shocking, interesting, even words cannot express this strange phenomenon: the concept of multi-threading has been known to magical engineering community long time before you arrived in this world.

"How can they already know such an advanced concept such as multi-threading when they still can't understand more basic concept of programming?", you ask that question with confusion and curiosity. Yet another strange phenomenon you find. Without waiting for another time, you find the Guild Master and ask about how this world already know such an advance concept.

"It seems you've done your study about non-serial magic. Well, normally, I don't know anything about magical engineering concept, but non-serial magic is different. This thing is very popular until at least ten years ago. Many things were created using this concept yet since ten years ago, it suddenly stopped being popular"

"Why did it suddenly happened?"

"Non-serial magic is powerful. It can do things that a serial magic can do two times, three times, even ten times faster, but very few magician can create it thanks to its complexity. Because of shortage of talented magician, we used serial magic again for our own use. Speaking of which, it seems The Magician's Association is in need of magician with knowledge of non-serial magic. Perhaps I can recommend you to them."

You are still not sure how to respond, yet this is the chance to understand more about many strange phenomenons that happened to you. Without hesitation, you accept the Guild Master's proposal.

## Multi-threading
You arrive at The Tower, the main headquarter of Magician's Association. You follow the Guild Master until you meet a certain person known as The Recruiter.

The Recruiter is no-nonsense person. Without giving you a proper greeting, The Recruiter gives you one of their programs. Here's the structure:
```
controller
	ManalithController.java
core
	Manalith.java
	Synthesis.java
	SynthesisCatalyst.java
	SynthesisKnight.java
repository
	SynthesisRepository.java
service
	ManalithService.java
	ManalithServiceImpl.java
Tutorial3Application.java
```
You look at the program and you find that the program is using multi-threading.

"It seems you are quite a talented magician. Yes, just as you said, this program uses multi-threading"

The Recruiter's approval give you a confidence that you will be accepted into the Association, yet you feel that just having his "approval" won't enough so you decide to examine the program.

You decide to run the program and open `localhost:8080/add_synthesis`. Apparently there are two type of `Synthesis`, which are `SynthesisKnight` and `SynthesisCatalyst`. `SynthesisKnight` can request mana from `Manalith` as long as the `Manalith` have the sufficient mana the `SynthesisKnight` asks it for. On the other hand, `SynthesisCatalyst` can offer their mana to the `Manalith` as long as they (the `SynthesisCatalyst`) have sufficient mana. The mana the `Synthesis` get depends on their `manaGain`. Their request will be placed inside a `Queue` inside the `Manalith`.

After you add some `Synthesis`, you decide to process the `Manalith`'s request by pushing the "Process Request" button. You find that the program runs incorrectly. The amount of mana the `Manalith` has is wrong, some requests are processed more than once, or sometimes are not even processed.

"Race condition", you mutter to yourself. A term that you suddenly remembered. It's probably the core reason the program fails to run correctly.

A closer look to your program it seems that the program doesn't handle race condition properly. There are many tasks that needs to be done to make sure this program runs correctly. Those tasks are:
1. Identify what part of program that cause the race condition.
2. Identify whether the data types and data structures used in the program are thread-safe or not.
3. Change the implementation so the program can run correctly despite the race condition (Hint: you might want to implement locking) (Caution : Your change should not change the functionality of the program)

Checklist
- [ ] Completing mandatory tasks
- [ ] BONUS: Clean the code
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU1NjI0NTEzOCwtMTUzODIyOTQyMCwxND
Q4NzMyMDQ2LC0xNDMzMjM0MDc5LC02NzA2NzI3MjgsLTIwNzg3
NjM4ODUsMTkxNTQ2MTI5Myw1ODAwMzAxNzgsLTE1NTc2NTE1NT
csLTIwMjgxNzA1NywxNTkxNTM3MDQxLDc5MjAyMTE5NSwtNjQ0
NTkwMDUxXX0=
-->