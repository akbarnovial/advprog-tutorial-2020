package id.ac.ui.cs.tutorial3.core;

public class SynthesisKnight extends Synthesis {
	public SynthesisKnight(String name, int mana){
		super(name, 1, mana);
	}

	public boolean respond(){
		this.mana = this.mana + this.manaGain;
		return true;
	}

	public String requestMessage(){
		return "Synthesis: " + this.name + " - Request Mana " + this.manaGain;
	}
}