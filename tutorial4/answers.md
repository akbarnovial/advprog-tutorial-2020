Quest 1:
Prometheus is used to monitor events and for alerting. It also records real time metrics in a time series database. 
we can check localhost:8080/actuator, inside we can see all the endpoints that the Actuator exposes such as /health. for example, we try "actuator/health", it will show that the status is up. 
the content of localhost:8000/actuator can be seen in image "actuator" 
![actuator](images/actuator.PNG)
![health](images/health.PNG)

Quest 2:
In this case, i used the variable add_seconds_count in prometheus. We need to go to HomeController.java and add the @Timed notation to the variable we want to check
 I went to localhost:8080 and then clicked on conference and add conference, inside I added 2 things to the conference list, when checked in prometheus in graph, it shows that it invokes the variable and shows that 2 things have been added to the count in variable "add_seconds_count".
![add_seconds_count](images/add_seconds_count.PNG)
The graph below is to show the maximum time to access the page
![add_seconds_max](images/add_seconds_max.PNG)
The graph below is for add_seconds_sum
![add_seconds_sum](images/add_seconds_sum.PNG)